var express = require("express");
var path = require('path');
var msql = require('../mysql/md_sql_com');


var router = express.Router();

router.get('/' , (req , res) =>{
    res.render('login');
});

router.post('/' , async (req , res) =>{
    
    let username = req.body.username;
    let password = req.body.password;

    var user = {
        fname   : "" ,
        lname   : "" , 
        title    : "" , 
        school  : "" , 
        err     : ""
    };

    let result = await msql.retrive_user(username , password);
    user.fname = result.frst_name;
    user.lname = result.last_name;
    user.school = result.school;
    user.title = result.title;
    console.log(user);

    res.render('index', { title: 'Jukebox' , username: `${username}` , fname: `${user.fname}` ,lname: `${user.lname}` , stat: `${user.title}` , school: `${user.school}` });

});

module.exports = router;