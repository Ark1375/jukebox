var express = require("express");
var path = require('path');
var msql = require('../mysql/md_sql_com');


var router = express.Router();

router.get('/' , (req , res) =>{
    res.render('register');
});

router.post('/' , (req , res) =>{

    msql.add_user(req.body.username , req.body.password , req.body.stchr , req.body.firstName , req.body.lastName , req.body.email , req.body.school);
    res.redirect('/');



});

module.exports = router;